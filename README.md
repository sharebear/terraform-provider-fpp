# Terraform Provider FPP

This repo contains my initial attempts at writing a terraform provider from scratch, inspired by the
[HashiCraft Holidays Hackstravaganza][HCHH] announcement, and the fact that I am starting to consider contributing some
PRs to the Azure provider after using it at $DAYJOB.

## What is FPP?

[Falcon Player][FPP] (previously Falcon Pi Player, hence the acronym), is a piece of software commonly used in the holiday
lighting community, for controlling "pixels" (individually addressable RGB LEDs). These LEDs could be connected directly
to the device where Falcon Player is running (a Raspberry Pi or Beagle Bone Black), or to a remote device that can be
remotely controlled via Falcon Player.

## How can Terraform be useful here?

While there have been advances in functionality of the [xLights][xLights] software for the remote configuration of
Falcon Player and other devices, some people do not yet trust the automatic configuration preferring to do it
themselves, this means entering data manually in a web interface, often based on spreadsheets. Moving this config to
Terraform would remove a factor of human error and allow tracking of configuration in source control.

## Goals

Goals for this project are;

* Learn some golang
* Gain better understanding of how Terraform providers work
* Submit as entry to the HashiCraft Holidays Hackstravaganza
* Retrieve status of Falcon Player instance (data source)
* Retrieve and update Test mode status (resource)
* Retrieve and update pixel string output (resource)
* Retrieve and update files (resource(s))

## Anti-goals

Things that are explicitly _not_ goals for this project are;

* Reusable golang api client for Falcon Players
* Perfectly idiomatic provider code

## Developing

### Build provider

```
$ make build
```

### Run unit tests

```
$ make test
```

### Install provider locally

```
$ make install
```

### Test sample configuration

```
$ cd examples/<choose an example>
$ rm -f .terraform.lock.hcl
$ terraform init && terraform apply
```

[HCHH]: https://www.hashicorp.com/blog/announcing-the-hashicraft-holidays-hackstravaganza
[FPP]: https://github.com/FalconChristmas/fpp
[xLights]: https://xlights.org/
