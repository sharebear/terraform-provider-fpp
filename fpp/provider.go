package fpp

import (
	"context"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	fpp "gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
	"gitlab.com/sharebear/terraform-provider-fpp/fpp/datasource"
	"gitlab.com/sharebear/terraform-provider-fpp/fpp/resource"
)

// Provider -
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"url": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "URL through which to access Falcon Player",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"fpp_channel_testing": resource.ResourceChannelTesting(),
			"fpp_sequence_file":   resource.ResourceSequenceFile(),
			"fpp_status":          resource.ResourceStatus(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"fpp_status": datasource.DataSourceStatus(),
		},
		ConfigureContextFunc: configureContext,
	}
}

func configureContext(ctx context.Context, data *schema.ResourceData) (interface{}, diag.Diagnostics) {
	baseUri := data.Get("url").(string)
	return fpp.NewFppClient(baseUri), nil
}
