package datasource

import (
	"context"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	fpp "gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
)

func DataSourceStatus() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceStatusRead,
		Schema: map[string]*schema.Schema{
			"fppd_status": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"scheduler_status": {
				Type:     schema.TypeString,
				Computed: true,
			},
			"player_status": {
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceStatusRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)

	var diags diag.Diagnostics

	status, err := fppClient.GetStatus(ctx)
	if err != nil {
		return diag.FromErr(err)
	}

	if err := d.Set("fppd_status", status.Fppd); err != nil {
		return diag.FromErr(err)
	}

	if err := d.Set("scheduler_status", status.Scheduler.Status); err != nil {
		return diag.FromErr(err)
	}

	if err := d.Set("player_status", status.StatusName); err != nil {
		return diag.FromErr(err)
	}

	// always run
	d.SetId(fppClient.GetBaseUri())

	return diags
}
