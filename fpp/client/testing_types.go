package client

type TestMode struct {
	Mode           string `json:"mode"`
	SubMode        string `json:"subMode,omitempty"`
	CycleMS        int    `json:"cycleMS,omitempty"`
	ColorPattern   string `json:"colorPattern,omitempty"`
	Color1         *int   `json:"color1,omitempty"`
	Color2         *int   `json:"color2,omitempty"`
	Color3         *int   `json:"color3,omitempty"`
	Enabled        uint8  `json:"enabled"`
	ChannelSet     string `json:"channelSet"`
	ChannelSetType string `json:"channelSetType"`
}
