package client

type Mqtt struct {
	Configured bool
	Connected  bool
}

type CurrentPlaylist struct {
	Count       string
	Description string
	Index       string
	Playlist    string
	Type        string
}

type NextPlaylist struct {
	Playlist  string
	StartTime string
}

type ScheduledNextPlaylist struct {
	PlaylistName          string
	ScheduledStartTime    int64
	ScheduledStartTimeStr string
}

type ScheduledCurrentPlaylist struct {
	PlaylistName string
}

type Scheduler struct {
	CurrentPlaylist ScheduledCurrentPlaylist
	NextPlaylist    ScheduledNextPlaylist
	Status          string
}

type Sensor struct {
	Formatted string
	Label     string
	Postfix   string
	Prefix    string
	Value     float64
	ValueType string
}

type Status struct {
	Mqtt             Mqtt
	CurrentPlaylist  CurrentPlaylist
	CurrentSequence  string
	CurrentSong      string
	Fppd             string
	Mode             int64
	ModeName         string
	NextPlaylist     NextPlaylist
	RepeatMode       string
	Scheduler        Scheduler
	SecondsPlayed    string
	SecondsRemaining string
	Sensors          []Sensor
	Status           int64
	StatusName       string `json:"status_name"`
	Time             string
	TimeElapsed      string
	TimeRemaining    string
	Volume           int64
}
