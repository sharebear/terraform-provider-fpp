package client

import (
	"encoding/xml"
)

type FileList struct {
	XMLName xml.Name `xml:"Files"`
	Files   []File   `xml:"File"`
}

type File struct {
	XMLName  xml.Name `xml:"File"`
	Name     string   `xml:"Name"`
	Time     string   `xml:"Time"`
	FileInfo string   `xml:"FileInfo"`
}

// http://fpp.local/fppxml.php?command=getFiles&dir=Sequences
// <Files></Files>

// curl 'http://fpp.local/jqupload.php' \
//  -H 'Connection: keep-alive' \
//  -H 'Pragma: no-cache' \
//  -H 'Cache-Control: no-cache' \
//  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
//  -H 'X-Requested-With: XMLHttpRequest' \
//  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36' \
//  -H 'Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryb2qhEH9qyQwvlwY6' \
//  -H 'Origin: http://fpp.local' \
//  -H 'Referer: http://fpp.local/uploadfile.php' \
//  -H 'Accept-Language: en-GB,en;q=0.9,nb;q=0.8,no;q=0.7,ru;q=0.6,sv;q=0.5' \
//  -H 'Cookie: PHPSESSID=fn21kb9vg82mfa1f4s33jgmgp2; LIMONADE0x5x0=f03k3mvrdncp7227nfdrji14ke' \
//  --data-binary $'------WebKitFormBoundaryb2qhEH9qyQwvlwY6\r\nContent-Disposition: form-data; name="myfile"; filename="StaticAnimation.fseq"\r\nContent-Type: application/octet-stream\r\n\r\n\r\n------WebKitFormBoundaryb2qhEH9qyQwvlwY6\r\nContent-Disposition: form-data; name=""\r\n\r\nundefined\r\n------WebKitFormBoundaryb2qhEH9qyQwvlwY6\r\nContent-Disposition: form-data; name=""\r\n\r\nundefined\r\n------WebKitFormBoundaryb2qhEH9qyQwvlwY6--\r\n' \
//  --compressed \
//  --insecure

// Response (type html)
// ["StaticAnimation.fseq"]

// http://fpp.local/fppxml.php?command=moveFile&file=StaticAnimation.fseq
// <Status>Success</Status>

// http://fpp.local/fppxml.php?command=getFiles&dir=Sequences
// <Files><File><Name>StaticAnimation.fseq</Name><Time>12/12/20  09:03 PM</Time><FileInfo>6.81KB</FileInfo></File></Files>
