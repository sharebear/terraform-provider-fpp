package client

import (
	"encoding/json"
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestMqttDeserialization(t *testing.T) {
	example := `{"configured":false,"connected":false}`

	var actual Mqtt
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	if actual.Configured != false {
		t.Errorf("Expected Configured to be false but was %+v", actual.Configured)
	}

	if actual.Connected != false {
		t.Errorf("Expected Connected to be false but was %+v", actual.Connected)
	}
}

func TestCurrentPlaylistDeserialization(t *testing.T) {
	example := `{"count":"0","description":"","index":"0","playlist":"","type":""}`

	var actual CurrentPlaylist
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := CurrentPlaylist{
		Count:       "0",
		Description: "",
		Index:       "0",
		Playlist:    "",
		Type:        "",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestNextPlaylist(t *testing.T) {
	example := `{"playlist":"No playlist scheduled.","start_time":""}`

	var actual NextPlaylist
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := NextPlaylist{
		Playlist:  "No playlist scheduled.",
		StartTime: "",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestScheduledNextPlaylist(t *testing.T) {
	example := `{"playlistName":"No playlist scheduled.","scheduledStartTime":0,"scheduledStartTimeStr":""}`

	var actual ScheduledNextPlaylist
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := ScheduledNextPlaylist{
		PlaylistName:          "No playlist scheduled.",
		ScheduledStartTime:    0,
		ScheduledStartTimeStr: "",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestScheduler(t *testing.T) {
	example := `{"nextPlaylist":{"playlistName":"No playlist scheduled.","scheduledStartTime":0,"scheduledStartTimeStr":""},"status":"idle"}`

	var actual Scheduler
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := Scheduler{
		NextPlaylist: ScheduledNextPlaylist{
			PlaylistName:          "No playlist scheduled.",
			ScheduledStartTime:    0,
			ScheduledStartTimeStr: "",
		},
		Status: "idle",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestSensor(t *testing.T) {
	example := `{"formatted":"41.9","label":"CPU: ","postfix":"","prefix":"","value":41.856,"valueType":"Temperature"}`

	var actual Sensor
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := Sensor{
		Formatted: "41.9",
		Label:     "CPU: ",
		Postfix:   "",
		Prefix:    "",
		Value:     41.856,
		ValueType: "Temperature",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestStatus(t *testing.T) {
	example := `{"MQTT":{"configured":false,"connected":false},"current_playlist":{"count":"0","description":"","index":"0","playlist":"","type":""},"current_sequence":"","current_song":"","fppd":"running","mode":2,"mode_name":"player","next_playlist":{"playlist":"No playlist scheduled.","start_time":""},"repeat_mode":"0","scheduler":{"nextPlaylist":{"playlistName":"No playlist scheduled.","scheduledStartTime":0,"scheduledStartTimeStr":""},"status":"idle"},"seconds_played":"0","seconds_remaining":"0","sensors":[{"formatted":"41.9","label":"CPU: ","postfix":"","prefix":"","value":41.856,"valueType":"Temperature"}],"status":0,"status_name":"idle","time":"Sun Dec 06 15:29:08 GMT 2020","time_elapsed":"00:00","time_remaining":"00:00","volume":70}`

	var actual Status
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := Status{
		Mqtt:            Mqtt{},
		CurrentPlaylist: CurrentPlaylist{},
		CurrentSequence: "",
		CurrentSong:     "",
		Fppd:            "running",
		Mode:            2,
		ModeName:        "",
		NextPlaylist:    NextPlaylist{},
		RepeatMode:      "",
		Scheduler: Scheduler{
			NextPlaylist: ScheduledNextPlaylist{
				PlaylistName:          "No playlist scheduled.",
				ScheduledStartTime:    0,
				ScheduledStartTimeStr: "",
			},
			Status: "idle",
		},
		SecondsPlayed:    "",
		SecondsRemaining: "",
		Sensors: []Sensor{{
			Formatted: "41.9",
			Label:     "CPU: ",
			Postfix:   "",
			Prefix:    "",
			Value:     41.856,
			ValueType: "Temperature",
		}},
		Status:        0,
		StatusName:    "idle",
		Time:          "Sun Dec 06 15:29:08 GMT 2020",
		TimeElapsed:   "",
		TimeRemaining: "",
		Volume:        70,
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestStatus_playing(t *testing.T) {
	example := `{
    "MQTT": {
        "configured": false,
        "connected": false
    },
    "current_playlist": {
        "count": "1",
        "description": "",
        "index": "1",
        "playlist": "StaticAnimation.fseq",
        "type": "sequence"
    },
    "current_sequence": "StaticAnimation.fseq",
    "current_song": "",
    "fppd": "running",
    "mode": 2,
    "mode_name": "player",
    "next_playlist": {
        "playlist": "No playlist scheduled.",
        "start_time": ""
    },
    "repeat_mode": 0,
    "scheduler": {
        "currentPlaylist": {
            "playlistName": "StaticAnimation.fseq"
        },
        "nextPlaylist": {
            "playlistName": "No playlist scheduled.",
            "scheduledStartTime": 0,
            "scheduledStartTimeStr": ""
        },
        "status": "manual"
    },
    "seconds_elapsed": "0",
    "seconds_played": "0",
    "seconds_remaining": "29",
    "sensors": [
        {
            "formatted": "41.9",
            "label": "CPU: ",
            "postfix": "",
            "prefix": "",
            "value": 41.856,
            "valueType": "Temperature"
        }
    ],
    "status": 1,
    "status_name": "playing",
    "time": "Thu Dec 17 20:32:50 GMT 2020",
    "time_elapsed": "00:00",
    "time_remaining": "00:29",
    "volume": 70
}
`

	var actual Status
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := Status{
		Mqtt:            Mqtt{},
		CurrentPlaylist: CurrentPlaylist{},
		CurrentSequence: "",
		CurrentSong:     "",
		Fppd:            "running",
		Mode:            2,
		ModeName:        "",
		NextPlaylist:    NextPlaylist{},
		RepeatMode:      "",
		Scheduler: Scheduler{
			CurrentPlaylist: ScheduledCurrentPlaylist{
				PlaylistName: "StaticAnimation.fseq",
			},
			NextPlaylist: ScheduledNextPlaylist{
				PlaylistName:          "No playlist scheduled.",
				ScheduledStartTime:    0,
				ScheduledStartTimeStr: "",
			},
			Status: "manual",
		},
		SecondsPlayed:    "",
		SecondsRemaining: "",
		Sensors: []Sensor{{
			Formatted: "41.9",
			Label:     "CPU: ",
			Postfix:   "",
			Prefix:    "",
			Value:     41.856,
			ValueType: "Temperature",
		}},
		Status:        1,
		StatusName:    "playing",
		Time:          "Thu Dec 17 20:32:50 GMT 2020",
		TimeElapsed:   "",
		TimeRemaining: "",
		Volume:        70,
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}
