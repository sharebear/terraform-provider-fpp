package client

import (
	"encoding/xml"
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestEmptyListOfFiles(t *testing.T) {
	apiExample := `<Files></Files>`

	fileList := FileList{}
	if err := xml.Unmarshal([]byte(apiExample), &fileList); err != nil {
		t.Errorf("Error parsing xml %+v", err)
	}

	if len(fileList.Files) != 0 {
		t.Errorf("Expected 0 files in the list but found %d", len(fileList.Files))
	}
}

func TestListOfFiles(t *testing.T) {
	apiExample := `
		<Files>
			<File><Name>StaticAnimation.fseq</Name><Time>12/12/20  09:03 PM</Time><FileInfo>6.81KB</FileInfo></File>
			<File><Name>BlinkyBlinky.fseq</Name><Time>12/12/20  09:05 PM</Time><FileInfo>6.22KB</FileInfo></File>
		</Files>`

	fileList := FileList{}
	if err := xml.Unmarshal([]byte(apiExample), &fileList); err != nil {
		t.Errorf("Error parsing xml %+v", err)
	}

	if len(fileList.Files) != 2 {
		t.Errorf("Expected 2 files in the list but found %d", len(fileList.Files))
	}

	actual := fileList.Files[0]
	expected := File{
		XMLName: xml.Name{
			Local: "File",
		},
		Name:     "StaticAnimation.fseq",
		Time:     "12/12/20  09:03 PM",
		FileInfo: "6.81KB",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}
