package client

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type FppClient struct {
	baseUri string
	http    *http.Client
}

func NewFppClient(baseUri string) FppClient {
	return FppClient{
		baseUri,
		&http.Client{Timeout: 10 * time.Second},
	}
}

func (client FppClient) GetBaseUri() string {
	return client.baseUri
}

func (client FppClient) GetStatus(ctx context.Context) (*Status, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/fppjson.php?command=getFPPstatus", client.baseUri), nil)
	if err != nil {
		return nil, err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	var status Status
	err = json.NewDecoder(r.Body).Decode(&status)
	if err != nil {
		return nil, err
	}

	return &status, nil
}

func (client FppClient) GetTestMode(ctx context.Context) (*TestMode, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/fppjson.php?command=getTestMode", client.baseUri), nil)
	if err != nil {
		return nil, err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	var current TestMode
	err = json.NewDecoder(r.Body).Decode(&current)
	if err != nil {
		return nil, err
	}

	return &current, nil
}

func (client FppClient) SetTestMode(ctx context.Context, testMode TestMode) error {
	b, err := json.Marshal(testMode)
	if err != nil {
		return err
	}

	data := url.Values{}
	data.Set("command", "setTestMode")
	data.Set("data", string(b))
	reader := strings.NewReader(data.Encode())

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/fppjson.php", client.baseUri), reader)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")

	r, err := client.http.Do(req)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	return nil
}

func (client FppClient) GetFileList(ctx context.Context) (*FileList, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/fppxml.php?command=getFiles&dir=Sequences", client.baseUri), nil)
	if err != nil {
		return nil, err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	var current FileList
	err = xml.NewDecoder(r.Body).Decode(&current)
	if err != nil {
		return nil, err
	}
	return &current, nil
}

// TODO: Is a string path to a file really a good API here or should I extract the file handle here?
func (client FppClient) UploadFile(ctx context.Context, fileName string) error {
	fileHandle, err := os.Open(fileName)
	if err != nil {
		return err
	}

	var b bytes.Buffer
	writer := multipart.NewWriter(&b)

	fileWriter, err := writer.CreateFormFile("myfile", fileName)
	if err != nil {
		return err
	}

	if _, err = io.Copy(fileWriter, fileHandle); err != nil {
		return err
	}

	if err := writer.Close(); err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", fmt.Sprintf("%s/jqupload.php", client.baseUri), &b)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	res, err := client.http.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", res.Status)
	}

	return nil
}

func (client FppClient) MoveFile(ctx context.Context, fileName string) error {
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/fppxml.php?command=moveFile&file=%s", client.baseUri, fileName), nil)
	if err != nil {
		return err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return err
	}

	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", r.Status)
	}

	return nil
}

func (client FppClient) StartPlaylist(ctx context.Context, playlistName string) error {
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/fppxml.php?command=startPlaylist&playList=%s&repeat=unchecked&playEntry=0&section=", client.baseUri, playlistName), nil)
	if err != nil {
		return err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return err
	}

	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", r.Status)
	}

	return nil
}

func (client FppClient) StopPlayer(ctx context.Context) error {
	// GET http://fpp.local/api/playlists/stop
	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/api/playlists/stop", client.baseUri), nil)
	if err != nil {
		return err
	}

	r, err := client.http.Do(req)
	if err != nil {
		return err
	}

	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", r.Status)
	}

	return nil
}
