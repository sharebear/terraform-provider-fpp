package client

import (
	"encoding/json"
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestTestModeDeserialization_RGBChase_enabled(t *testing.T) {
	example := `{"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"FF000000FF000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}`

	var actual TestMode
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := TestMode{
		Mode:           "RGBChase",
		SubMode:        "RGBChase-RGB",
		CycleMS:        1000,
		ColorPattern:   "FF000000FF000000FF",
		Enabled:        1,
		ChannelSet:     "1-8",
		ChannelSetType: "channelRange",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestTestModeSerialization_RGBChase_enabled(t *testing.T) {
	input := TestMode{
		Mode:           "RGBChase",
		SubMode:        "RGBChase-RGB",
		CycleMS:        1000,
		ColorPattern:   "FF000000FF000000FF",
		Enabled:        1,
		ChannelSet:     "1-8",
		ChannelSetType: "channelRange",
	}

	actual, err := json.Marshal(&input)
	if err != nil {
		t.Fatal(err)
	}

	expected := `{"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"FF000000FF000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}`

	if diff := cmp.Diff(expected, string(actual)); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestTestModeDeserialization_RGBFill_enabled(t *testing.T) {
	example := `{"mode":"RGBFill","color1":255,"color2":0,"color3":255,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}`

	var actual TestMode
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	red := 255
	green := 0
	blue := 255

	expected := TestMode{
		Mode:           "RGBFill",
		Color1:         &red,
		Color2:         &green,
		Color3:         &blue,
		Enabled:        1,
		ChannelSet:     "1-8",
		ChannelSetType: "channelRange",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestTestModeSerialization_RGBFill_enabled(t *testing.T) {
	red := 255
	green := 0
	blue := 255

	input := TestMode{
		Mode:           "RGBFill",
		Color1:         &red,
		Color2:         &green,
		Color3:         &blue,
		Enabled:        1,
		ChannelSet:     "1-8",
		ChannelSetType: "channelRange",
	}

	actual, err := json.Marshal(&input)
	if err != nil {
		t.Fatal(err)
	}

	expected := `{"mode":"RGBFill","color1":255,"color2":0,"color3":255,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}`

	if diff := cmp.Diff(expected, string(actual)); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestTestModeDeserialization_disabled(t *testing.T) {
	example := `{ "enabled": 0 }`

	var actual TestMode
	if err := json.Unmarshal([]byte(example), &actual); err != nil {
		t.Fatal(err)
	}

	expected := TestMode{
		Enabled: 0,
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}
