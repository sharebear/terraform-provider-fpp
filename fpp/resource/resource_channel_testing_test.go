package resource

import (
	"github.com/google/go-cmp/cmp"
	"gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
	"testing"
)

func TestUnflattenTestMode_disabled(t *testing.T) {
	input := ResourceChannelTesting().TestResourceData()

	input.Set("enabled", false)

	actual := unflattenTestMode(input)

	expected := client.TestMode{
		Enabled: 0,
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestUnflattenTestMode_channelrange(t *testing.T) {
	input := ResourceChannelTesting().TestResourceData()
	input.Set("enabled", true)
	input.Set("channel_range", []map[string]int{
		{"start": 5, "end": 200},
	})
	input.Set("rgb_chase", []map[string]interface{}{
		{
			"mode":     "RGB",
			"cycle_ms": 1000,
		},
	})

	actual := unflattenTestMode(input)

	expected := client.TestMode{
		Enabled:        1,
		Mode:           "RGBChase",
		SubMode:        "RGBChase-RGB",
		CycleMS:        1000,
		ColorPattern:   "FF000000FF000000FF",
		ChannelSetType: "channelRange",
		ChannelSet:     "5-200",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
}

func TestUnflattenTestMode_RGBChase_RGB(t *testing.T) {
	input := ResourceChannelTesting().TestResourceData()
	input.Set("enabled", true)
	input.Set("channel_range", []map[string]int{
		{"start": 1, "end": 300},
	})
	input.Set("rgb_chase", []map[string]interface{}{
		{
			"mode":     "RGB",
			"cycle_ms": 1000,
		},
	})

	actual := unflattenTestMode(input)

	expected := client.TestMode{
		Enabled:        1,
		Mode:           "RGBChase",
		SubMode:        "RGBChase-RGB",
		CycleMS:        1000,
		ColorPattern:   "FF000000FF000000FF",
		ChannelSetType: "channelRange",
		ChannelSet:     "1-300",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}

	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"FF000000FF000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"FF00000000FF00FF00","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"00FF00FF00000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"0000FFFF000000FF00","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"00FF000000FFFF0000","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGB","cycleMS":1000,"colorPattern":"0000FF00FF00FF0000","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBChase_RGBA(t *testing.T) {
	// {"mode":"RGBChase","subMode":"RGBChase-RGBA","cycleMS":1000,"colorPattern":"FF000000FF000000FFFFFFFF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"RGBChase","subMode":"RGBChase-RGBA","cycleMS":1000,"colorPattern":"FF00000000FF00FF00FFFFFF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBChase_RGBN(t *testing.T) {
	// {"mode":"RGBChase","subMode":"RGBChase-RGBN","cycleMS":1000,"colorPattern":"FF000000FF000000FF000000","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBChase_RGBAN(t *testing.T) {
	// {"mode":"RGBChase","subMode":"RGBChase-RGBAN","cycleMS":1000,"colorPattern":"FF000000FF000000FFFFFFFF000000","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBChase_RGBCustom(t *testing.T) {
	input := ResourceChannelTesting().TestResourceData()
	input.Set("enabled", true)
	input.Set("channel_range", []map[string]int{
		{"start": 1, "end": 300},
	})
	input.Set("rgb_chase", []map[string]interface{}{
		{
			"mode":           "Custom",
			"custom_pattern": "1563FF000000623CE4000000",
			"cycle_ms":       500,
		},
	})

	actual := unflattenTestMode(input)

	expected := client.TestMode{
		Enabled:        1,
		Mode:           "RGBChase",
		SubMode:        "RGBChase-Custom",
		CycleMS:        500,
		ColorPattern:   "1563FF000000623CE4000000",
		ChannelSetType: "channelRange",
		ChannelSet:     "1-300",
	}

	if diff := cmp.Diff(expected, actual); diff != "" {
		t.Errorf("Deserialization mismatch (-expected, +actual):\n%s", diff)
	}
	// {"mode":"RGBChase","subMode":"RGBChase-RGBCustom","cycleMS":1000,"colorPattern":"FF000000FF000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBCycle_RGB(t *testing.T) {
	// {"mode":"RGBCycle","subMode":"RGBCycle-RGB","cycleMS":1000,"colorPattern":"FF000000FF000000FF","enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_RGBFill(t *testing.T) {
	// {"mode":"RGBFill","color1":255,"color2":255,"color3":255,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}

func TestUnflattenTestMode_SingleChase(t *testing.T) {
	// {"mode":"SingleChase","cycleMS":1000,"chaseSize":2,"chaseValue":255,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"SingleChase","cycleMS":1000,"chaseSize":2,"chaseValue":165,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
	// {"mode":"SingleChase","cycleMS":1000,"chaseSize":6,"chaseValue":165,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"} max chase size
}

func TestUnflattenTestMode_SingleChannelFill(t *testing.T) {
	// {"mode":"RGBFill","color1":196,"color2":196,"color3":196,"enabled":1,"channelSet":"1-8","channelSetType":"channelRange"}
}
