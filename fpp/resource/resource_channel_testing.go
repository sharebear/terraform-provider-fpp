package resource

import (
	"context"
	"fmt"
	"github.com/hashicorp/go-cty/cty"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/validation"
	fpp "gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
	"regexp"
	"strconv"
	"strings"
)

func ResourceChannelTesting() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceChannelTestingCreateUpdate,
		ReadContext:   resourceChannelTestingRead,
		UpdateContext: resourceChannelTestingCreateUpdate,
		DeleteContext: resourceChannelTestingDelete,
		Schema: map[string]*schema.Schema{
			"enabled": {
				Type:        schema.TypeBool,
				Required:    true,
				Description: "If true, enables the channel test mode",
			},
			"channel_range": {
				Type:     schema.TypeList,
				Required: true,
				MaxItems: 1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"start": {
							Type:         schema.TypeInt,
							Optional:     true,
							Default:      1,
							ValidateFunc: validation.IntAtLeast(1),
						},
						"end": {
							Type:         schema.TypeInt,
							Required:     true,
							ValidateFunc: validation.IntAtLeast(1),
						},
					},
				},
			},
			"rgb_chase": {
				Type:         schema.TypeList,
				Optional:     true,
				ExactlyOneOf: []string{"rgb_chase", "rgb_cycle", "rgb_fill"},
				MaxItems:     1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"mode": {
							Type:         schema.TypeString,
							Required:     true,
							ValidateFunc: validation.StringInSlice([]string{"RGB", "RGBA", "RGBN", "RGBAN", "Custom"}, false),
						},
						"custom_pattern": {
							Type:             schema.TypeString,
							Optional:         true,
							ValidateDiagFunc: validatePatternString,
						},
						"cycle_ms": {
							Type:     schema.TypeInt,
							Optional: true,
							Default:  1000,
						},
					},
				},
			},
			"rgb_cycle": {
				Type:         schema.TypeList,
				Optional:     true,
				ExactlyOneOf: []string{"rgb_chase", "rgb_cycle", "rgb_fill"},
				MaxItems:     1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"mode": {
							Type:         schema.TypeString,
							Required:     true,
							ValidateFunc: validation.StringInSlice([]string{"RGB", "RGBA", "RGBN", "RGBAN", "Custom"}, false),
						},
						"custom_pattern": {
							Type:             schema.TypeString,
							Optional:         true,
							ValidateDiagFunc: validatePatternString,
						},
						"cycle_ms": {
							Type:     schema.TypeInt,
							Optional: true,
							Default:  1000,
						},
					},
				},
			},
			"rgb_fill": {
				Type:         schema.TypeList,
				Optional:     true,
				ExactlyOneOf: []string{"rgb_chase", "rgb_cycle", "rgb_fill"},
				MaxItems:     1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"red": {
							Type:         schema.TypeInt,
							Required:     true,
							ValidateFunc: validation.IntBetween(0, 255),
						},
						"green": {
							Type:         schema.TypeInt,
							Required:     true,
							ValidateFunc: validation.IntBetween(0, 255),
						},
						"blue": {
							Type:         schema.TypeInt,
							Required:     true,
							ValidateFunc: validation.IntBetween(0, 255),
						},
					},
				},
			},
		},
	}
}

func validatePatternString(pattern interface{}, path cty.Path) diag.Diagnostics {
	var diags diag.Diagnostics

	patternString, ok := pattern.(string)
	if !ok {
		diags = append(diags, diag.Diagnostic{
			Severity:      diag.Error,
			Summary:       "Pattern must be a string",
			Detail:        "Unsure if I really need to validate this as it should be enforced by the schema",
			AttributePath: path,
		})
	}

	length := len(patternString)
	if length == 0 {
		diags = append(diags, diag.Diagnostic{
			Severity:      diag.Error,
			Summary:       "Empty pattern",
			Detail:        "Pattern cannot be empty when set",
			AttributePath: path,
		})
	}

	if length%6 != 0 {
		diags = append(diags, diag.Diagnostic{
			Severity:      diag.Error,
			Summary:       "Incorrect pattern length",
			Detail:        fmt.Sprintf("Pattern length must be a multiple of 6 (red, green and blue values in hex) but was %d", length),
			AttributePath: path,
		})
	}

	// TODO: This currently doesn't support lower case a-f, I need to check what the API supports. If the API can return
	// lowercase then I need to normalize to avoid incorrect diff.
	r := regexp.MustCompile("[0-9A-F].*")
	regexOk := r.MatchString(patternString)
	if !regexOk {
		diags = append(diags, diag.Diagnostic{
			Severity:      diag.Error,
			Summary:       "Invalid character in pattern",
			Detail:        fmt.Sprintf("Pattern must contain only valid hex characters, received [%s]", patternString),
			AttributePath: path,
		})
	}

	return diags
}

func resourceChannelTestingCreateUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)

	var diags diag.Diagnostics

	testMode := unflattenTestMode(d)

	fppClient.SetTestMode(ctx, testMode)

	d.SetId("static")

	resourceChannelTestingRead(ctx, d, m)

	return diags
}

var colorPatternDict = map[string]string{
	"RGB":   "FF000000FF000000FF",
	"RGBA":  "FF000000FF000000FFFFFFFF",
	"RGBN":  "FF000000FF000000FF000000",
	"RGBAN": "FF000000FF000000FFFFFFFF000000",
}

func unflattenTestMode(d *schema.ResourceData) fpp.TestMode {
	if d.Get("enabled").(bool) {

		// No error checking here as we are trusting Required and MaxItems to do their job
		channelRanges := d.Get("channel_range").([]interface{})
		channelRange := channelRanges[0].(map[string]interface{})
		channelSet := fmt.Sprintf("%d-%d", channelRange["start"], channelRange["end"])

		rgbFills := d.Get("rgb_fill").([]interface{})
		if rgbFills != nil && len(rgbFills) > 0 {
			rgbFill := rgbFills[0].(map[string]interface{})
			red := rgbFill["red"].(int)
			green := rgbFill["green"].(int)
			blue := rgbFill["blue"].(int)
			return fpp.TestMode{
				Mode:           "RGBFill",
				Color1:         &red,
				Color2:         &green,
				Color3:         &blue,
				Enabled:        1,
				ChannelSet:     channelSet,
				ChannelSetType: "channelRange",
			}
		}

		var apiMode string
		var subMode string
		var cycleMs int
		var colorPattern string

		rgbChases := d.Get("rgb_chase").([]interface{})
		if rgbChases != nil && len(rgbChases) > 0 {
			rgbChase := rgbChases[0].(map[string]interface{})
			apiMode = "RGBChase"
			schemaMode := rgbChase["mode"].(string)

			subMode = fmt.Sprintf("%s-%s", apiMode, schemaMode)

			if schemaMode != "Custom" {
				colorPattern = colorPatternDict[schemaMode]
			} else {
				colorPattern = rgbChase["custom_pattern"].(string)
			}

			cycleMs = rgbChase["cycle_ms"].(int)
		}

		rgbCycles := d.Get("rgb_cycle").([]interface{})
		if rgbCycles != nil && len(rgbCycles) > 0 {
			rgbCycle := rgbCycles[0].(map[string]interface{})
			apiMode = "RGBCycle"
			schemaMode := rgbCycle["mode"].(string)

			subMode = fmt.Sprintf("%s-%s", apiMode, schemaMode)

			if schemaMode != "Custom" {
				colorPattern = colorPatternDict[schemaMode]
			} else {
				colorPattern = rgbCycle["custom_pattern"].(string)
			}

			cycleMs = rgbCycle["cycle_ms"].(int)
		}

		return fpp.TestMode{
			Mode:           apiMode,
			SubMode:        subMode,
			CycleMS:        cycleMs,
			ColorPattern:   colorPattern,
			Enabled:        1,
			ChannelSet:     channelSet,
			ChannelSetType: "channelRange",
		}
	}

	return fpp.TestMode{
		Enabled: 0,
	}
}

func resourceChannelTestingRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)

	var diags diag.Diagnostics

	current, err := fppClient.GetTestMode(ctx)
	if err != nil {
		return diag.FromErr(err)
	}

	if err := d.Set("enabled", current.Enabled > 0); err != nil {
		return diag.FromErr(err)
	}

	if len(current.ChannelSetType) > 0 {

		if current.ChannelSetType != "channelRange" {
			return diag.Errorf("Provider currently only supports 'channelRange' for the 'channelSetType' but found '%s'", current.ChannelSetType)
		}

		split := strings.Split(current.ChannelSet, "-")

		startChannel, err := strconv.Atoi(split[0])
		if err != nil {
			return diag.FromErr(err)
		}
		endChannel, err := strconv.Atoi(split[1])
		if err != nil {
			return diag.FromErr(err)
		}

		if err := d.Set("channel_range", []map[string]int{
			{"start": startChannel, "end": endChannel},
		}); err != nil {
			return diag.FromErr(err)
		}
	}

	mode := current.Mode
	if mode == "RGBChase" {
		if err := d.Set("rgb_chase", []map[string]interface{}{
			{
				"mode":           current.SubMode[9:],
				"custom_pattern": current.ColorPattern,
				"cycle_ms":       current.CycleMS,
			},
		}); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_cycle", nil); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_fill", nil); err != nil {
			return diag.FromErr(err)
		}
	}

	if mode == "RGBCycle" {
		if err := d.Set("rgb_cycle", []map[string]interface{}{
			{
				"mode":           current.SubMode[9:],
				"custom_pattern": current.ColorPattern,
				"cycle_ms":       current.CycleMS,
			},
		}); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_chase", nil); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_fill", nil); err != nil {
			return diag.FromErr(err)
		}
	}

	if mode == "RGBFill" {
		if err := d.Set("rgb_fill", []map[string]interface{}{
			{
				"red":   current.Color1,
				"green": current.Color2,
				"blue":  current.Color3,
			},
		}); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_chase", nil); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rgb_cycle", nil); err != nil {
			return diag.FromErr(err)
		}
	}

	return diags
}

func resourceChannelTestingDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	if err := d.Set("enabled", false); err != nil {
		return diag.FromErr(err)
	}

	var diags diag.Diagnostics

	resourceChannelTestingRead(ctx, d, m)

	return diags
}
