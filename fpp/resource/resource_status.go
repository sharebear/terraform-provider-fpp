package resource

import (
	"context"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/validation"
	fpp "gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
)

func ResourceStatus() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceStatusCreate,
		ReadContext:   resourceStatusRead,
		UpdateContext: resourceStatusCreate,
		DeleteContext: resourceStatusDelete,
		Schema: map[string]*schema.Schema{
			"player_status": {
				Type:         schema.TypeString,
				Required:     true,
				ValidateFunc: validation.StringInSlice([]string{"idle", "playing"}, false),
			},
			"playlist": {
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func resourceStatusDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	return diag.Errorf("DELETE NOT YET IMPLEMENTED")
}

func resourceStatusCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)
	var diags diag.Diagnostics

	// TODO : Validate sequence name exists

	desiredStatus := d.Get("player_status")
	if desiredStatus == "idle" {
		if err := fppClient.StopPlayer(ctx); err != nil {
			return diag.FromErr(err)
		}
	} else if desiredStatus == "playing" {
		if err := fppClient.StartPlaylist(ctx, d.Get("playlist").(string)); err != nil {
			return diag.FromErr(err)
		}
	}

	resourceStatusRead(ctx, d, m)

	return diags
}

func resourceStatusRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)

	var diags diag.Diagnostics

	status, err := fppClient.GetStatus(ctx)
	if err != nil {
		return diag.FromErr(err)
	}

	playerStatus := status.StatusName
	if err := d.Set("player_status", playerStatus); err != nil {
		return diag.FromErr(err)
	}

	if playerStatus == "playing" {
		if err := d.Set("playlist", status.Scheduler.CurrentPlaylist.PlaylistName); err != nil {
			return diag.FromErr(err)
		}
	}

	// always run
	d.SetId(fppClient.GetBaseUri())

	return diags
}
