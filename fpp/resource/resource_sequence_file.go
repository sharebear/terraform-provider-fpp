package resource

import (
	"context"
	"fmt"
	"github.com/hashicorp/go-cty/cty"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	fpp "gitlab.com/sharebear/terraform-provider-fpp/fpp/client"
	"strings"
)

func ResourceSequenceFile() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceSequenceFileCreate,
		ReadContext:   resourceSequenceFileRead,
		DeleteContext: resourceSequenceFileDelete,
		Importer: &schema.ResourceImporter{
			StateContext: resourceSequenceFileImport,
		},
		Schema: map[string]*schema.Schema{
			"filename": {
				Type:             schema.TypeString,
				Required:         true,
				ForceNew:         true, // Probably only temporary, perhaps update makes sense?
				ValidateDiagFunc: validateSequenceFilename,
			},
		},
	}
}

func resourceSequenceFileImport(ctx context.Context, d *schema.ResourceData, m interface{}) ([]*schema.ResourceData, error) {
	diags := resourceSequenceFileRead(ctx, d, m)
	if len(diags) < 0 {
		return nil, fmt.Errorf("found errors importing %+v", diags)
	}

	return []*schema.ResourceData{d}, nil
}

func resourceSequenceFileDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	return diag.Errorf("DELETE NOT YET IMPLEMENTED")
}

func resourceSequenceFileCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)

	var diags diag.Diagnostics

	current, err := fppClient.GetFileList(ctx)
	if err != nil {
		return diag.FromErr(err)
	}

	fileName := d.Get("filename").(string)
	fileDetails := findFileWithName(current, fileName)

	if fileDetails != nil {
		return diag.Errorf("File with name %s already exists", fileName)
	}

	if err := fppClient.UploadFile(ctx, fileName); err != nil {
		return diag.FromErr(err)
	}

	if err := fppClient.MoveFile(ctx, fileName); err != nil {
		return diag.FromErr(err)
	}

	d.SetId(fileName)

	resourceSequenceFileRead(ctx, d, m)

	return diags
}

// http://fpp.local/fppxml.php?command=getFiles&dir=Sequences
func resourceSequenceFileRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	fppClient := m.(fpp.FppClient)
	var diags diag.Diagnostics

	current, err := fppClient.GetFileList(ctx)
	if err != nil {
		return diag.FromErr(err)
	}

	fileName := d.Id()
	fileDetails := findFileWithName(current, fileName)

	if fileDetails != nil {
		if err := d.Set("filename", fileDetails.Name); err != nil {
			diag.FromErr(err)
		}

		if err := d.Set("time", fileDetails.Time); err != nil {
			diag.FromErr(err)
		}

		if err := d.Set("file_info", fileDetails.FileInfo); err != nil {
			diag.FromErr(err)
		}

		d.SetId(fileName)
	} else {
		d.SetId("")
	}

	return diags
}

func findFileWithName(fileList *fpp.FileList, fileName string) *fpp.File {
	for _, n := range fileList.Files {
		if fileName == n.Name {
			return &n
		}
	}
	return nil
}

func validateSequenceFilename(fileName interface{}, path cty.Path) diag.Diagnostics {
	var diags diag.Diagnostics

	fileNameString, ok := fileName.(string)
	if !ok {
		return diag.Errorf("Expected value to be a string")
	}

	if !(strings.HasSuffix(fileNameString, ".fseq") || strings.HasSuffix(fileNameString, ".fseq.gz")) {
		return diag.Errorf("Sequence filename must end in .fseq or .fseq.gz")
	}

	// TODO: We should probably validate existence of the file here too

	return diags
}
