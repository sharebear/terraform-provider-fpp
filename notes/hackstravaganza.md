# Hackstravaganza Submission

Submission video must explain the following points

## What you have built

1. Explain what Falcon Player is
    * How?

2. Demonstrate click-ops
    * Input and output configuration
    * File Upload
    * Playlist creation
    * Potentially lots of work if your SD card goes bad with no backup

3. Demo examples
    * Status datasource
    * Testing resource
    * File upload resource
    * Status resource (Perhaps save that for "What is cool?")

## Why you have built it

1. Fun way to start learning golang (still prefer Rust ;))
2. Get familiar with provider development before attempting to scratch some itches with the azurerm provider
3. Internet stardom via HashiCraft Holidays Hackstravaganza

## What is cool about it

1. Treat holiday lighting controllers as cattle not pets
2. Code as ugly as your holiday sweater!
3. Makes terraform sing and dance!
