# Hashitalks 2021

## Please provide a talk title

Keeping the (holiday) lights on with Terraform

## Please provide a talk abstract

In 2008 Simone Giertz challenged us to ["make useless things"][1]. When I saw the announcement for the HashiCraft
Holidays Hackstravaganza I was inspired to step up to the challenge by creating a Terraform provider for Falcon
Player, a tool popular in the holiday lighting community.

In this talk I will walk through how I approached developing a Terraform provider having never written a
Terraform provider before or experience writing golang.

Through this talk I hope to inspire others to make "useless" providers, giving them a few tips on how to get started.

[1]: https://www.ted.com/talks/simone_giertz_why_you_should_make_useless_things

---
Non public section

Apologies for the late entry, talk planning wasn't on the top of my agenda the last two weeks.

Talk is based upon my entry here: https://discuss.hashicorp.com/t/team-sharebear/18559 I've not yet decided if I'll
use the tree in the talk or just refer to the previous video, probably depends on how quickly I can land the slide deck.

Tentative talk structure (timing _very_ rough based on last minute planning)
* Context (5-10 mins)
  * Intro + background
  * How I stumbled across the Hackstravaganza announcement
  * Brainstorming + what is Falcon Player
* Implementation (10-15 mins)
  * Getting started (copy example)
  * Set project scope
  * Start with the types (intro to mitmproxy + json de-serialisation in golang)
  * Implement a vertical slice (data source, to verify connectivity)
  * It's ok to hardcode values (first attempt at resource)
* Conclusion (5 mins)
  * What have I learnt?
    * Enough golang to start contributing PRs and reviewing other's PRs
    * Better understanding of how/why certain bugs/features occur
      * Concrete example: resources not getting added to state even though they were created due to error handling when
        waiting for completion
  * What have you learnt?
    * Start small, you don't need to support every property from the start
    * Code doesn't need to be pretty (at least not yet)
    * mitmproxy is great for inspecting traffic
    * Making "useless" providers, isn't a big challenge


---

Original notes

* Can repeat some points from Hackstravaganza entry video
* Simone Giertz: Why you should make useless things https://www.ted.com/talks/simone_giertz_why_you_should_make_useless_things
    * TODO: Actually watch the talk
* Did I learn anything useful?
    * Don't be afraid of copy-pasting ugly code to get results... refactoring/testing can come later
    * mitmproxy is awesome for exploratory investigation of "APIs" and quick debugging of the provider
    * Don't forget to delete existing state
    * API design choices
    * TODO: Brainstorm when I have time...
