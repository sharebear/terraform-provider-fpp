#!/usr/bin/env bash

set -e

rm -f presentation/terraform.tfstate

cp presentation/main.tf.01 presentation/main.tf
terraform -chdir=presentation init
terraform -chdir=presentation apply -auto-approve
echo "Push enter for RGB Chase"
read
cp presentation/main.tf.02 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for Custom Chase"
read
cp presentation/main.tf.03 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for FASTER Custom Chase"
read
cp presentation/main.tf.04 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for Cycle"
read
cp presentation/main.tf.05 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for static color"
read
cp presentation/main.tf.06 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for less lights"
read
cp presentation/main.tf.07 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for Animation"
read
cp presentation/main.tf.08 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "Push enter for Carol of the bells"
read
cp presentation/main.tf.09 presentation/main.tf
terraform -chdir=presentation apply -auto-approve
echo "FADE TO BLACK (and breathe out)"
