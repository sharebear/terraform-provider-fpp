# Status Data Source Example

Quick example demonstrating usage of the `fpp_status` data source.

A clean apply against an idle instance returns the following output

```
$ terraform apply

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

status = {
  "fppd_status" = "running"
  "id" = "http://fpp.local"
  "player_status" = "idle"
  "scheduler_status" = "idle"
}
```

If I then activate the channel testing test mode via the web ui and run apply again I get the following

```
$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:

Terraform will perform the following actions:

Plan: 0 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  ~ status = {
      ~ player_status    = "idle" -> "testing"
        # (3 unchanged elements hidden)
    }

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes


Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

status = {
  "fppd_status" = "running"
  "id" = "http://fpp.local"
  "player_status" = "testing"
  "scheduler_status" = "idle"
}
```
