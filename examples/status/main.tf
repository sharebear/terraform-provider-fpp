terraform {
    required_providers  {
        fpp = {
        source = "gitlab.com/sharebear/fpp"
        }
    }
}

provider "fpp" {
    url = "http://localhost:9001"
}

data "fpp_status" "main" {
}

output "status" {
    value = data.fpp_status.main
}
