terraform {
  required_providers {
    fpp = {
      source = "gitlab.com/sharebear/fpp"
    }
  }
}

provider "fpp" {
  url = "http://localhost:9001"
}

resource "fpp_sequence_file" "static_animation" {
  filename = "StaticAnimation.fseq"
}
