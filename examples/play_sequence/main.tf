terraform {
  required_providers {
    fpp = {
      source = "gitlab.com/sharebear/fpp"
    }
  }
}

provider "fpp" {
  url = "http://localhost:9001"
}

resource "fpp_sequence_file" "static_animation" {
  filename = "StaticAnimation.fseq"
}

resource "fpp_status" "main" {
  player_status = "idle"
  playlist = fpp_sequence_file.static_animation.id
}
