terraform {
  required_providers {
    fpp = {
      source = "gitlab.com/sharebear/fpp"
    }
  }
}

provider "fpp" {
  url = "http://localhost:9001"
}

resource "fpp_channel_testing" "main" {
  enabled = false

  channel_range {
    end = 150
  }

  rgb_fill {
    red   = 98
    green = 60
    blue  = 228
  }
}

resource "fpp_sequence_file" "animation" {
  filename = "StaticAnimation.fseq"
}

resource "fpp_sequence_file" "carol" {
  filename = "CarolOfTheBells.fseq"
}

resource "fpp_status" "main" {
  player_status = "playing"
  playlist      = fpp_sequence_file.carol.id
}
