module gitlab.com/sharebear/terraform-provider-fpp

go 1.14

require (
	github.com/google/go-cmp v0.5.4
	github.com/hashicorp/go-cty v1.4.1-0.20200414143053-d3edf31b6320
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.3.0
)
